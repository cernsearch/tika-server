FROM openjdk:8-jre

# Part of the IT-CDA-WF Section
MAINTAINER Ismael Posada Trobo <ismael.posada.trobo@cern.ch>

ENV TIKA_VERSION 1.14

ENV TIKA_SERVER_URL https://archive.apache.org/dist/tika/tika-server-$TIKA_VERSION.jar

RUN apt-get update
RUN apt-get install curl -y
RUN curl "$TIKA_SERVER_URL" -s -o tika-server-${TIKA_VERSION}.jar
RUN apt-get clean -y

EXPOSE 9998

CMD java -jar /tika-server-${TIKA_VERSION}.jar -h 0.0.0.0