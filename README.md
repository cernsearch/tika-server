# Tika Server

The Apache Tika toolkit detects and extracts metadata and text from over a thousand different file types (such as PPT, XLS, and PDF). All of these file types can be parsed through a single interface, making Tika useful for search engine indexing, content analysis, translation, and much more.

# How to deploy
- `oc new-app <url_to_this_repository>`
- `oc expose svc/tika-server`